#Free Social Sharer

Una sencilla página para compartir en redes sociales libres.

##Uso

En tu sitio web crea un botón con la siguiente url yoursite.com/sharer/index.html/?text=tu texto&url=tu url donde text es el texto que desees postear y url la url del post.

Si deseas usar un webservice para actualizar la base de datos con los clicks, añade a la url &articleid={id} donde id es la id única para actualizar tu tabla.

**Usa urlencode o encodeURIComponent dependiendo de si usas js o php para codificar esa variable, de lo contrario se romperán las urls que usen ampersand**

##Licencia

Esta movida está escrita bajo la licencia gnu gpl v3 se libre de copiar, modificar y distribuir. Be water my friend ;)

##Demos

Demo en Castellano: http://darkpractices.net/sharer/index.html?text=Texto&url=URL%20a%20compartir

Demo en Català: http://darkpractices.net/sharer/ca/index.html?text=Texte&url=URL%20a%20comparteix

Demo in English: http://darkpractices.net/sharer/en/index.html?text=Text&url=Shared%20URL