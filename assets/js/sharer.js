/*! sharer v1.2.5.M2 by Makhiscal | (c) Copyleft 2018 | GNU/GPL v3 */
/*
@licstart  The following is the entire license notice for the
JavaScript code in this page.

Copyright (C) 2018  Kim

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.  


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/

//configuration
var w = 650; //modal window width
var h = 500; //modal window height
var webservice = ''; //webservice url to update database with sharer hits, pass the article id to the button url {articleid}

function GetURLParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
		    return sParameterName[1];
		}
	}
}

$(document).ready(function() {

	//get version from manifest
	$.getJSON('manifest.webmanifest', function(data) {
		$.each(data, function(index, element) {
		    $('#version').html(data.version);
		});
	});

	//get url parameter
	var varText = GetURLParameter('text');
	var varUrl  = GetURLParameter('url');
	var varId   = GetURLParameter('articleid');
	$('.text').val(decodeURIComponent(varText));
	$('.url').val(decodeURIComponent(varUrl));

	//get browser database
	var mastodon 	= localStorage.getItem('Mastodon');
	var diaspora 	= localStorage.getItem('Diaspora');
	var gnusocial 	= localStorage.getItem('Gnusocial');
	var friendica 	= localStorage.getItem('Friendica');
	var hubzilla 	= localStorage.getItem('Hubzilla');
	var movim 		= localStorage.getItem('Movim');
	var pleroma 	= localStorage.getItem('Pleroma');

	if(mastodon != null) {
		$('#hostMastodon').val(mastodon);
		$('.mastodon').removeAttr('disabled');
	}
	if(diaspora != null) {
		$('#hostDiaspora').val(diaspora);
		$('.diaspora').removeAttr('disabled');
	}
	if(gnusocial != null) {
		$('#hostGnusocial').val(gnusocial);
		$('.gnusocial').removeAttr('disabled');
	}
	if(friendica != null) {
		$('#hostHubzilla').val(hubzilla);
		$('.hubzilla').removeAttr('disabled');
	}
	if(hubzilla != null) {
		$('#hostFriendica').val(friendica);
		$('.friendica').removeAttr('disabled');
	}
	if(movim != null) {
		$('#hostMovim').val(movim);
		$('.movim').removeAttr('disabled');
	}
	if(pleroma != null) {
		$('#hostPleroma').val(pleroma);
		$('.pleroma').removeAttr('disabled');
	}

	$('.host').keyup(function() {
		var service = $(this).closest("form").attr('data-service');

		if(service == 'Mastodon') {
			$('.mastodon').removeAttr('disabled');
		}
		if(service == 'Diaspora') {
			$('.diaspora').removeAttr('disabled');
		}
		if(service == 'Gnusocial') {
			$('.gnusocial').removeAttr('disabled');
		}
		if(service == 'Friendica') {
			$('.friendica').removeAttr('disabled');
		}
		if(service == 'Hubzilla') {
			$('.hubzilla').removeAttr('disabled');
		}
		if(service == 'Movim') {
			$('.movim').removeAttr('disabled');
		}
		if(service == 'Pleroma') {
			$('.pleroma').removeAttr('disabled');
		}
	});

	//open new window
	$('.send').click(function(e) {
		e.preventDefault();

		var site  = $(this).attr('data-site');
		var check = $('#remember'+site);

		if(check.is(':checked')) {
			var host = $('#host'+site).val();
			localStorage.setItem(site, host);
		}

	  	var text    = $('#text'+site).val();
	  	var host    = $('#host'+site).val();
	  	var url     = $('#url'+site).val();
	  	var action  = '';
	  	var service = $(this).closest("form").attr('data-service');

	  	if(host != '' && text != '') {

	  		//add protocol https
	  		var prefix = 'https://';
			if (host.substr(0, prefix.length) !== prefix)
			{
				host = prefix + host;
			}

			if(service == 'Mastodon') {
				action = host+'/share?text='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);	
				$('.mastodon').removeAttr('disabled');
			}
			if(service == 'Diaspora') {
				action = host+'/bookmarklet?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(text)+'&jump=doclose';
				$('.diaspora').removeAttr('disabled');
			}
			if(service == 'Gnusocial') {
				action = host+'/?action=newnotice&status_textarea='+encodeURIComponent(text)+' '+encodeURIComponent(url);
				$('.gnusocial').removeAttr('disabled');
			}
			if(service == 'Friendica') {
				action = host+'/bookmarklet?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(text)+'&jump=doclose';
				$('.friendica').removeAttr('disabled');
			}
			if(service == 'Hubzilla') {
				action = host+'/rpost?body='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);
				$('.hubzilla').removeAttr('disabled');
			}
			if(service == 'Movim') {
				action = host+'/?share&text='+encodeURIComponent(text)+'&url='+encodeURIComponent(url);
				$('.movim').removeAttr('disabled');
			}
			if(service == 'Pleroma') {
				action = host+'/notice/new?status_textarea='+encodeURIComponent(text)+' '+encodeURIComponent(url);
				$('.pleroma').removeAttr('disabled');
			}
	  	}

	  	//execute webservice if any
	  	if(webservice != '') {
	  		$.post(webservice, {id: varId});
	  	}

		var left = Number((screen.width/2)-(w/2));
		var top  = Number((screen.height/2)-(h/2));

		window.open(action, '_blank', 'width='+w+',height='+h+',left='+left+',top='+top+'');
	});
});
